@extends('layout')

@include('error')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3>{{$task->title}}</h3>
            <hr>
            <p>
                {{$task->description}}
            </p>
        </div>
    </div>
</div>
@endsection


