<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chocolife</title>
    <link href="https://fonts.googleapis.com/css?family=Vollkorn:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

    <!--HEADER-->

<!---<header id="header" class="header nav-link">
    <div class="container">
        <div class="ml-auto">
            <nav>
                <ul class="menu d-flex justify-content-center">
                    <li class="menu__item">
                        <a class="link-color" href="#">
                            All items
                        </a>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Top rated
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Most sales
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            New
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Old
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Archive
                        </a>
                    </li>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>

    <!--STICKY NAVBAR-->

<nav id="nav" class="navbar navbar-expand-lg navbar-dark sticky-top">
    <div class="container">
        <a class="link-logo" href="#">
            ChocoLogo
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <li class="nav-item dropdown my-auto">
                <a class="nav-link text-light small dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Choose your city    <span class="badge badge-light">2</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Almaty</a>
                    <a class="dropdown-item" href="#">Astana</a>
                </div>
            </li>
            <form class="form-inline my-2 mx-md-auto my-lg-0">
                <input type="text" class="form-control mr-sm-2">
                <button class="btn btn-outline-color my-2 my-sm-0">Search</button>
            </form>
            <div class="ml-auto">
                <button type="button" class="btn text-color btn-link">Sign in</button>
                <button type="button" class="btn text-light btn-link">Sign up</button>
            </div>
        </div>
    </div>
</nav>

    <!--jumbortron-->

    <div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Sales, Offers and all you need</h1>
        <p class="lead">All you need is here. Just find it!</p>
        <hr class="my-4">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi dignissimos facere ipsum itaque laboriosam
            libero nesciunt porro quisquam tenetur? Assumenda commodi eveniet mollitia nobis porro provident quia
            reprehenderit sequi veniam!</p>
        <p class="lead">
            <a class="btn btn-outline-color2 btn-lg" href="#" role="button">Learn more</a>
        </p>
    </div>
</div>

    <!--action1 section1-->

<section class="actions" id="actions1">
    <div class="container">
        <div class="row">
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action1.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 1  <span class="badge badge-secondary">New</span></h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="offer1.html" class="card-link">View more</a>
                    </div>
                </div>
            </div>
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action2.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 2  <span class="badge badge-secondary">New</span></h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="offer2.html" class="card-link">View more</a>
                    </div>
                </div>
            </div>
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action3.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 3</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="#" class="card-link">View more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!--top-action section2-->

    <section class="top-action mt-3" id="top-action">
    <div class="container">
        <div class="card mb-3">
            <img class="card-img-top" src="assets/banner.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title text-center">Scpecial offer for building materials</h5>
                <p class="card-text">With building materials, as with most things we buy, the less expensive something
                    is initially, the shorter its life-span usually is.</p>
                <div class="card-footer">
                    <div class="text-center">
                        <a href="#" class="card-link">
                            Learn more about building materials
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--action2 section3-->

<section class="actions" id="actions2">
    <div class="container">
        <div class="row">
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action4.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 4</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="offer4.html" class="card-link">View more</a>
                    </div>
                </div>
            </div>
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action5.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 5</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="#" class="card-link">View more</a>
                    </div>
                </div>
            </div>
            <div class="col text-light">
                <div class="card mt-3">
                    <img src="assets/action6.jpg" alt="ation1" class="card-img-top">
                    <div class="card-body text-center text-dark">
                        <h5 class="card-title">Offer 6</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                            beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                            quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                        <a href="#" class="card-link">View more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!--action3 section4-->

    <section class="actions" id="actions3">
        <div class="container">
            <div class="row">
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action7.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 7  <span class="badge badge-secondary">New</span></h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="offer1.html" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action8.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 8  <span class="badge badge-secondary">New</span></h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="offer2.html" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action9.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 9</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="#" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--action4 section5-->

    <section class="actions" id="actions4">
        <div class="container">
            <div class="row">
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action10.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 10  <span class="badge badge-secondary">New</span></h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="offer1.html" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action11.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 11     <span class="badge badge-secondary">New</span></h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="offer2.html" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
                <div class="col text-light">
                    <div class="card mt-3">
                        <img src="assets/action12.jpg" alt="ation1" class="card-img-top">
                        <div class="card-body text-center text-dark">
                            <h5 class="card-title">Offer 12</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam,
                                beatae eius excepturi ipsum laboriosam maiores neque, non odit officia pariatur possimus
                                quaerat recusandae similique sunt ullam vel vero voluptatem!</p>
                            <a href="#" class="card-link">View more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--pagining section6-->

<section class="pagining" id="pagining">
    <nav aria-label="navigation">
        <ul class="pagination pagination-lg mt-4 justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
</section>

    <!--footer-->

<footer class="mainfooter mt-4" role="contentinfo">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <!--Column1-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">Address</h4>
                        <address>
                            <ul class="list-unstyled">
                                <li>
                                    City Hall<br>
                                    Groove Street <br>
                                    California<br>
                                    635<br>
                                </li>
                                <li>Phone: +7-701-785-2022</li>
                            </ul>
                        </address>
                    </div>
                </div>
                <!--Column2-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">About us</h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Our company</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>  Instagram</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook"></i>  Facebook</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-github"></i>  Github</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Column3-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">Our aplications</h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <i class="fab fa-android"></i>  Android</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-apple"></i>  IOS</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-windows"></i>  Windows Phone</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Column4-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">For partners</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Popular departments</a></li>
                            <li><a href="#">For your business</a></li>
                            <li><a href="#">Popular services</a></li>
                            <li><a href="#">Get started</a></li>
                            <li><a class="pl-2 nav-link p-0" href="#">
                                <i class="fab fa-medapps"></i>
                                Need help?</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer Bottom-->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-center">&copy; Copyright 2018 - Almaty. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>